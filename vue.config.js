module.exports = {
    devServer: {
        proxy: {
            '/community':{
                target: 'http://localhost:8081/',
                changeOrigin: true,
            }
        }
    }
}
