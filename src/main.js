import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from 'vue-router'
import Routes from "./routers.js"
import axios from "axios";
import VueCookies from "vue-cookies";

Vue.prototype.axios = axios
Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.use(VueCookies)
const router = new VueRouter({
    routes: Routes,
    mode: 'history'
})

Vue.config.productionTip = false

new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
}).$mount('#app')
