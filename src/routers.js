import Home from "./views/Home"
import Registry from "@/views/Registry";
import Login from "@/views/Login";
import DiscussPostDetail from "@/views/DiscussPostDetail"
import Message from "@/views/Message";
import MessageDetail from "@/views/MessageDetail";
import Profile from "@/views/Profile";
import NotFound from "@/views/404";
import Search from "@/views/Search";

export default [
    {path: "/", component: Home,},
    {path: "/registry", component: Registry},
    {path: '/login', component: Login,},
    {path: '/postDetail', component: DiscussPostDetail},
    {path: '/message', component: Message},
    {path: '/messageDetail', component: MessageDetail},
    {path: '/profile', component: Profile},
    {path: '/search', component: Search},
    {path: '*', name: '404', component: NotFound},
]
